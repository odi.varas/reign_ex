import React, { useEffect, useState } from "react";
import { isToday, isYesterday } from "date-fns";
import axios from "axios";
import "./Body.css";
import { format } from "date-fns/esm";

import trash from "../Media/img/delete.png";

const Body = () => {
  const [stories, setStories] = useState([]);

  useEffect(() => {
    const url = `http://localhost:8001/news/`;
    console.log(url);
    axios
      .get(url)
      .then((res) => {
        const orderData = res.data.sort((storyA, storyB) =>
          storyA.created_at_i > storyB.created_at_i ? -1 : 1
        );
        setStories(orderData);
      })
      .catch((err) => alert(err));
  }, []);

  const deleteNews = async (id) => {
    let storiesCopy = [...stories];
    storiesCopy = storiesCopy.filter((story) => story._id !== id);
    await axios
      .delete(`http://localhost:8001/news/${id}`)
      .then(() => setStories(storiesCopy))
      .catch((err) => alert(err));
  };
  return (
    <div className="container">
      {stories.map((story) => (
        <InfoRow key={story._id} data={story} deleteNew={deleteNews} />
      ))}
    </div>
  );
};

export default Body;

const InfoRow = ({ data, deleteNew }) => {
  const [showTrash, setShowTrash] = useState(false);
  const {
    author,
    story_title,
    title,
    url,
    story_url,
    created_at_i,
    _id,
  } = data;

  return (
    <div
      className="container__row"
      onMouseEnter={() => setShowTrash(true)}
      onMouseLeave={() => setShowTrash(false)}
    >
      <a
        href={story_url || url}
        target="_blank"
        rel="noreferrer"
        className="container__rowLink"
      >
        <div className="container__rowLeft">
          <span className="container__rowLeftTitle">
            {story_title || title}
          </span>
          <span className="container__rowLeftAuthor">- {author} -</span>
        </div>

        <div className="container__rowCenter">
          <span className="container__rowRightDate">
            {formatDate(created_at_i)}
          </span>
        </div>
      </a>
      <div className="container__rowRight">
        <div className="container__rowRightTrash">
          {showTrash ? (
            <img
              src={trash}
              alt="trash"
              className="container__rowRightTrashIma"
              onClick={() => deleteNew(_id)}
            />
          ) : null}
        </div>
      </div>
    </div>
  );
};

const formatDate = (timestamp) => {
  const date = new Date(timestamp * 1000);
  if (isToday(date)) {
    return format(date, "KK':'mm aaa").toLowerCase();
  } else if (isYesterday(date)) {
    return "Yesterday";
  } else {
    return format(date, "LLL dd");
  }
};
