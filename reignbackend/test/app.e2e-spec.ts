import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';

const app = 'http://localhost:8001';

describe('AppController (e2e)', () => {
  // beforeAll(async () => {
  //   const url = `mongodb://mongo:27017/HNews`;
  //   await mongoose.connect(url, { useNewUrlParser: true });
  // });

  describe('ROOT', () => {
    it('should not response', () => {
      return request(app).get('/').expect(404);
    });
  });

  describe('NEWS', () => {
    it('should update from API', () => {
      return request(app)
        .get('/news/updateDataServer')
        .expect(200)
        .expect(({ body }) => {
          expect(body).toBeInstanceOf(Array);
        });
    });

    it('should get a valid answer', () => {
      return request(app)
        .get('/news')
        .expect(200)
        .expect(({ body }) => {
          expect(body).toBeInstanceOf(Array);
          // expect(body.length).toBeGreaterThanOrEqual(0);
          body.map((value) => {
            expect(value).toHaveProperty('_id');
            expect(value).toHaveProperty('author');
            expect(value).toHaveProperty('story_title');
            expect(value).toHaveProperty('title');
            expect(value).toHaveProperty('url');
            expect(value).toHaveProperty('story_url');
            expect(value).toHaveProperty('deleted', false);
          });
        });
    });
  });

  it('should get error when ""delete"" something that doesnt exist ', () => {
    const id = `000000000`;
    return request(app).delete(`/news/${id}`).expect(404);
  });
});
