import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NewsModule } from './hackersNews/hackersNews.module';

@Module({
  imports: [
    NewsModule,
    MongooseModule.forRoot(
      // 'mongodb+srv://adminReign:Reign.01!@reigncluster.5uvjw.mongodb.net/HNews?retryWrites=true&w=majority',
      'mongodb://mongo:27017/HNews?retryWrites=true&w=majority',
    ),
    ScheduleModule.forRoot(),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
