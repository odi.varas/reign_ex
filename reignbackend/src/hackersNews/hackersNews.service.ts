import {
  InternalServerErrorException,
  HttpService,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';
import { Model } from 'mongoose';

import { HackersNews } from './hackersNews.model';

@Injectable()
export class HackersNewsService {
  constructor(
    @InjectModel('hackersNews')
    private readonly hackersNewsModel: Model<HackersNews>,
    private http: HttpService,
  ) {}

  async getAllNews() {
    const allHackersNews = await this.hackersNewsModel
      .find({ deleted: false })
      .sort('-create_at_i')
      .exec();

    return allHackersNews.filter(
      (hNews) => hNews.title || hNews.story_title,
    ) as HackersNews[];
  }

  async deleteHackersNews(idHackersNews: number) {
    let hackersNews;
    try {
      hackersNews = await this.hackersNewsModel.findById(idHackersNews);
    } catch (error) {
      throw new NotFoundException('News not found');
    }
    if (!hackersNews) {
      throw new NotFoundException('News not found');
    }
    hackersNews.deleted = true;
    hackersNews.save();
  }

  @Cron('0 * * * * *')
  async updateHackersNews() {
    const url = `http://hn.algolia.com/api/v1/search_by_date?query=nodejs`;
    const dataFromUrl = await this.http
      .get(url)
      .toPromise()
      .then((res) => {
        return res.data;
      })
      .catch((err) => {
        throw new InternalServerErrorException();
      });

    const dataFetchFromUrl: HackersNews[] = dataFromUrl.hits.map((item) => {
      const {
        objectID,
        author,
        created_at_i,
        title,
        story_title,
        url,
        story_url,
      } = item;
      const document = {
        _id: parseInt(objectID),
        created_at_i,
        author,
        story_title,
        title,
        url,
        story_url,
        deleted: false,
      };
      return document;
    });

    const idDataFetchFromUrl: number[] = dataFetchFromUrl.map(
      (item) => item._id,
    );
    const idDataAlreadyExists: number[] = (
      await this.hackersNewsModel.find({
        _id: {
          $in: idDataFetchFromUrl,
        },
      })
    ).map((item) => item._id);

    const dataToSave: HackersNews[] = dataFetchFromUrl.filter(
      (item) => !idDataAlreadyExists.includes(item._id),
    );
    if (dataToSave.length > 0) {
      await this.hackersNewsModel.insertMany(dataToSave);
    }
    return dataToSave;
  }
}
