import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { HackersNewsService } from './hackersNews.service';

@Controller('news')
export class NewsController {
  constructor(private readonly newsService: HackersNewsService) {}

  @Get('updateDataServer')
  async updateHackersNews() {
    return this.newsService.updateHackersNews();
  }

  @Get()
  async getAllNews() {
    return this.newsService.getAllNews();
  }

  @Delete(':id')
  async deleteHackersNews(@Param('id') idHackNews: number) {
    await this.newsService.deleteHackersNews(idHackNews);
    return null;
  }
}
