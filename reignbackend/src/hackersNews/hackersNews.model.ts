import * as mongoose from 'mongoose';

export const HackersNewSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  created_at_i: Number,
  author: String,
  story_title: String,
  title: String,
  url: String,
  story_url: String,
  deleted: Boolean,
});

export interface HackersNews extends mongoose.Document {
  _id: number;
  created_at_i: number;
  author: string;
  story_title: string;
  title: string;
  url: string;
  story_url: string;
  deleted: boolean;
}
