import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsController } from './hackersNews.controller';

import { HackersNewSchema } from './hackersNews.model';
import { HackersNewsService } from './hackersNews.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'hackersNews', schema: HackersNewSchema },
    ]),
    HttpModule,
  ],
  controllers: [NewsController],
  providers: [HackersNewsService],
})
export class NewsModule {}
