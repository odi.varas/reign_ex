# REIGN Challenge

## Requirements

Lastest Version of [Docker](https://www.docker.com/).

## Set up

After cloning the repo, just execute the following lines

```
cd reign_ex
docker-compose up
```

With the commands above, we will set up our backend and frontend, with the followers characteristics:

### Backend

Running latest version of MongoDB [running on port 27017]

An API Server composed of Node.js [latest lts version]+ NestJs. This API will be mounted on localhost port 8001 [link](http://localhost:8001/), and will expose 3 endpoints.

- [GET] /news/updateDataServer : With this endpoint will fetch the data from API of hackers News manually.
- [GET] /news : will return all hackers News on mongodb that hasnt been deleted before.
- [DELETE] /news/:id : will mark as delete a hackers news.

### Frontend

An app builded on latest version of React [17.0.1]. Using axios to communicate with backend. This app will be mounted on localhost port 3000

## Hints

After getting all running and ready to go. I will highly recommend to trigger an initial pull of the data from hacks news using this [link](http://localhost:8001/news/updateDataServer) to populate de DB.

Kind regards,
Nelson
